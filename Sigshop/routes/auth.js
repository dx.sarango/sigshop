const express = require("express");
const jwt = require("jsonwebtoken");

const mongoose = require("../config/database");

const router = express.Router();

const { dbverification } = require("../middleware/db");

import UserSchema from "../models/user";
const User = mongoose.main_conn.model("User", UserSchema);
const User2 = mongoose.admin_conn.model("User", UserSchema);

router.post("/login", dbverification, async (req, res) => {
  const body = req.body;
  try {
    const userDB = await User.findOne({ email: body.email });

    if (!userDB) {
      return res.status(400).json({
        message: "Usuario incorrecto"
      });
    }

    if (!(body.pass === userDB.pass)) {
      return res.status(400).json({
        message: "Usuario incorrecto"
      });
    }

    let token = jwt.sign(
      {
        data: userDB
      },
      "secret",
      { expiresIn: 60 * 60 * 24 * 30 }
    );

    res.json({
      userDB,
      token
    });
  } catch (error) {
    return res.status(400).json({
      message: "Ocurrio un error",
      error
    });
  }
});

router.post("/register", async (req, res) => {
  const body = req.body;
  try {
    const userDB = await User.create(body);
    const userBU = await User2.create(body);
    res.json(userDB);
  } catch (error) {
    return res.status(500).json({
      message: "Ocurrio un error",
      error
    });
  }
});

module.exports = router;
