const express = require("express");

import products from "./products";
import categories from "./categories";
import auth from "./auth";

const router = express.Router();

router.use("/auth", auth);
router.use("/products", products);
router.use("/categories", categories);

module.exports = router;
