import express from "express";
import morgan from "morgan";
import cors from "cors";
import path from "path";

import mongoose from "./config/database";

import { dbverification } from "./middleware/db";

const app = express();

// var minutes = 1,
//   the_interval = minutes * 60 * 1000;
// setInterval(function() {
//   console.log("I am doing my 1 minutes check");
// }, the_interval);

// Conexion a base de datos
// const uri = "mongodb://localhost:27017/sigshop";

// const options = {
//   useNewUrlParser: true,
//   useCreateIndex: true,
//   useUnifiedTopology: true,
//   useFindAndModify: false
// };

// mongoose.connect(uri, options).then(
//   () => {
//     console.log("Conectado a DB1");
//   },
//   err => {
//     console.log(err);
//   }
// );

// Middleware
app.use(morgan("tiny"));
app.use(cors());
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true
  })
);

// Rutas del server
app.use("/api", require("./routes"));

// Middleware para Vue.js router modo history
const history = require("connect-history-api-fallback");
app.use(history());
app.use(express.static(path.join(__dirname, "public")));

app.set("port", process.env.PORT || 3000);
app.listen(app.get("port"), () => {
  console.log("Listening on port: " + app.get("port"));
});
