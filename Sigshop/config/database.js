const mongoose = require("mongoose");
const uri = "mongodb://localhost:27017/sigshop";
const uri2 = "mongodb://localhost:27017/sigshop2";

const options = {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: false
};

mongoose.main_conn = mongoose.createConnection(uri, options);
mongoose.admin_conn = mongoose.createConnection(uri2, options);

module.exports = exports = mongoose;
