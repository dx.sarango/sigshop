const jwt = require("jsonwebtoken");

const verification = (req, res, next) => {
  let token = req.get("token");
  jwt.verify(token, "secret", (err, decoded) => {
    if (err) {
      return res.status(401).json({
        message: "Error de token",
        err
      });
    }
    req.user = decoded.data;
    next();
  });
};

module.exports = {
  verification
};
